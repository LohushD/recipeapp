import UIKit
class RecipeCell : UITableViewCell {
  @IBOutlet weak var title: UILabel!
  @IBOutlet weak var socialRank: UILabel!
  @IBOutlet weak var dishImage: UIImageView!
}