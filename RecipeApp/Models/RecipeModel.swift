import JSONJoy

class Recipes{
  var recipes : [RecipeModel] = []
  required init(_ decoder: JSONDecoder) {
    if let rec = decoder["recipes"].array {
      recipes = [RecipeModel]()
      for recDecoder in rec {
        recipes.append(RecipeModel(recDecoder))
      }
    }
  }
}

class RecipeModel : JSONJoy {
  let title: String
  let imageUrl: String
  let socialRank : Double
  let recipeId : Int
  
  required init(_ decoder: JSONDecoder) {
    title = decoder["title"].string!
    imageUrl = decoder["image_url"].string!
    socialRank = decoder["social_rank"].double!
    recipeId = Int(decoder["recipe_id"].string!, radix:16)!
  }
}

class DetailedRecipeModel : RecipeModel{
  var ingredients: [String]
  let publisher : String
  
  required init(_ decoder: JSONDecoder) {
    ingredients = [String]()
    if let ingrs = decoder["ingredients"].array {
      for ingrDecoder in ingrs {
        ingredients.append(ingrDecoder.string!)
      }
    }
    publisher = decoder["publisher"].string!
    super.init(decoder)
  }
}