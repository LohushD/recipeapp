import Foundation
import Alamofire
import JSONJoy

class DataProvider {
  
  static var request: Alamofire.Request?
  
  class func getRecipes(name: String, page: Int, success: ((recipes: Recipes) -> Void)) {
    request?.cancel()
    request = Alamofire.request(Router.GetRecipes(name: name, page: page))
      .response { response in
        if let data = response.2 {
          success(recipes: Recipes(JSONDecoder(data)))
        }
    }
  }
  
  class func getRecipe(recipeId : String, success: ((recipe: DetailedRecipeModel) -> Void)) {
    Alamofire.request(Router.GetRecipe(recipeId: recipeId))
      .response { response in
        if let data = response.2 {
          success(recipe: DetailedRecipeModel(JSONDecoder(data)["recipe"]))
        }
    }
  }
  
  class func getImage(imageURL : String, success: ((image: UIImage) -> Void)){
    Alamofire.request(.GET, imageURL)
      .response { response in
        if let data = response.2,
          let image = UIImage(data: data, scale:1) {
            success(image: image)
        }
    }
  }  
}