//
//  Router.swift
//  RecipeApp
//
//  Created by Dmytro Lohush on 12/4/15.
//  Copyright © 2015 Dmytro Lohush. All rights reserved.
//
import Alamofire

enum Router: URLRequestConvertible {
  static let baseURLString = "http://food2fork.com/api"
  static let consumerKey = "f94b6301b2e3c830d6d633f3411abaf1"
  
  case GetRecipes(name: String, page: Int)
  case GetRecipe(recipeId: String)
  
  var path: String {
    switch self {
    case .GetRecipes:
      return "/search"
    case .GetRecipe:
      return "/get"
    }
  }
  
  var URLRequest: NSMutableURLRequest {
    let URL = NSURL(string: Router.baseURLString)!
    let mutableURLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(path))
    
    let params: [String : String] = {
      switch self {
      case .GetRecipes(let name, let page):
        return [
          "key" : Router.consumerKey,
          "q" : name,
          "page" : "\(page)"
        ]
      case .GetRecipe(let recipeId):
        return [
          "key" : Router.consumerKey,
          "rId" : recipeId
        ]
      }
    }()
    return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: params).0
  }
}