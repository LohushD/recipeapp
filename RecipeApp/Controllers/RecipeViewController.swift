import UIKit

class RecipeViewController : UIViewController{
  @IBOutlet weak var dishImage: UIImageView!
  @IBOutlet weak var recipeTitle: UILabel!
  @IBOutlet weak var rating: UILabel!
  @IBOutlet weak var publisher: UILabel!
  @IBOutlet weak var ingredients: UILabel!
  var recipeId : String?
  
  override func viewDidLoad() {
    
    DataProvider.getRecipe(recipeId!) {recipe in
      
      DataProvider.getImage(recipe.imageUrl){
        image in
        self.dishImage.image = image
      }
      self.recipeTitle.text = recipe.title
      self.rating.text = "Rating: \(Int(recipe.socialRank))"
      self.publisher.text = "Publisher: \(recipe.publisher)"
      for ingredient in recipe.ingredients{
        self.ingredients.text?.appendContentsOf("\n\(ingredient)")
      }
    }
  }
}