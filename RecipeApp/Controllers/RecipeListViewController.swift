import UIKit
import Alamofire

class RecipeListViewController: UIViewController{
  
  @IBOutlet weak var searchBar: UISearchBar!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
  var recipeList : Recipes?
  var loadingRecipes = false
  var currentPage = 1
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setDelegates()
  }
  
  func loadRecipes() {
    if loadingRecipes {
      return
    }
    loadingRecipes = true
    DataProvider.getRecipes(self.searchBar.text!, page: self.currentPage+1){
      recipes in
      let lastItem = self.recipeList!.recipes.count
      self.recipeList?.recipes += recipes.recipes
      let indexPaths = (lastItem..<self.recipeList!.recipes.count).map { NSIndexPath(forItem: $0, inSection: 0) }
      self.tableView!.insertRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.Automatic)
      self.loadingRecipes = false
      self.currentPage++
    }
  }
  
  private func setDelegates(){
    tableView.delegate = self
    tableView.dataSource = self
    searchBar.delegate = self
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
    if (segue.identifier == "singleRecipeSegue") {
      if let vc = segue.destinationViewController as? RecipeViewController {
        vc.recipeId = String(recipeList!.recipes[(sender as! NSIndexPath).row].recipeId, radix: 16)
      }
    }
  }
  
  func searchRecipe(recipe : String) {
    DataProvider.getRecipes(recipe, page: self.currentPage){recipes in
      self.currentPage = 1
      self.recipeList = recipes
      self.tableView.reloadData()
      self.loadingIndicator.stopAnimating()
    }
  }
}

//MARK: - UITableViewDataSource
extension RecipeListViewController : UITableViewDataSource{
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if self.recipeList!.recipes.count - indexPath.row < 10{
      loadRecipes()
    }
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! RecipeCell
    cell.title?.text = recipeList?.recipes[indexPath.row].title;
    cell.socialRank?.text = "Rating: \(Int(recipeList!.recipes[indexPath.row].socialRank))";
    cell.dishImage?.image = nil
    DataProvider.getImage((recipeList?.recipes[indexPath.row].imageUrl)!){
      image in
      cell.dishImage.image = image
    }
    
    return cell;
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if recipeList == nil {
      return 0
    }else{
      return (recipeList?.recipes.count)!
    }
  }
}

//MARK: - UITableViewDelegate
extension RecipeListViewController : UITableViewDelegate{
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    self.performSegueWithIdentifier("singleRecipeSegue", sender: indexPath)
  }
}

//MARK: - UISearchBarDelegate
extension RecipeListViewController : UISearchBarDelegate{
  
  func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
    if searchText.characters.count == 0{
      recipeList?.recipes.removeAll()
      self.tableView.reloadData()
    }
    searchRecipe(searchText)
    loadingIndicator.startAnimating()
  }
}
